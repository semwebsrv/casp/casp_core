#!groovy

podTemplate(
  containers:[ 
    containerTemplate(name: 'jdk11',                image:'adoptopenjdk:11-jdk-openj9',   ttyEnabled:true, command:'cat'),
    containerTemplate(name: 'docker',               image:'docker:18',                    ttyEnabled:true, command:'cat') 
  ],
  volumes: [
    hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
  ])
{
  node(POD_LABEL) {
  
    stage ('checkout') {
      checkout_details = checkout scm
    }
  
    stage ('build domain model') {
      container('jdk11') {
        dir ('service') {
          sh './gradlew --version'
          sh './gradlew --no-daemon --console=plain clean build'
        }
      }
    }
  
    // https://www.jenkins.io/doc/book/pipeline/docker/
    stage('Build Docker Image') {
      container('docker') {
        //sh 'ls service/build/libs'
        docker_image = docker.build("casp_core_service")
      }
    }
  
  
  
    // This build produces an executable war file of the form casp_core-1.0.0-SNAPSHOT.war 
    // Bundle it as a docker container
    stage('Publish Docker Image') {
      container('docker') {
        def props = readProperties file: './service/gradle.properties'
        constructed_tag = "build-${props?.version}-${checkout_details?.GIT_COMMIT?.take(12)}"

        println("Considering build tag : ${constructed_tag}");
        // Some interesting stuff here https://github.com/jenkinsci/pipeline-examples/pull/83/files
        docker.withRegistry('http://docker.semweb.co', 'semweb-nexus') {
          docker_image.push("latest")
          // myapp.push("${env.BUILD_ID}")
        }
      }
    }


    stage('Rolling Update') {
        kubernetesDeploy(
          // Credentials for k8s to run the required deployment commands
          kubeconfigId: 'kubeconfig2',
          // Definition of the deployment
          configs: 'deployment.yml',
          // Where to pull the image from
          dockerCredentials: [
            [ url:'http://docker.semweb.co', credentialsId:'semweb-nexus' ]
          ]
        )
    }

  }

}
