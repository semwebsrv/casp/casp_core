// Place your Spring DSL code here
import capcreator.CCUserDetailsService
import holo.core.user.UserPasswordEncoderListener
import capcreator.CAPGraphqlContextBuilder

beans = {
  userPasswordEncoderListener(UserPasswordEncoderListener)
  userDetailsService(capcreator.CCUserDetailsService)
  graphqlContextBuilder(CAPGraphqlContextBuilder)
}
