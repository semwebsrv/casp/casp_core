import grails.util.BuildSettings
import grails.util.Environment
import org.springframework.boot.logging.logback.ColorConverter
import org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter

import java.nio.charset.StandardCharsets

conversionRule 'clr', ColorConverter
conversionRule 'wex', WhitespaceThrowableProxyConverter

// logger ('grails.app.init', DEBUG)
// logger ('grails.app.controllers', DEBUG)
logger ('grails.app.domains', DEBUG)
// logger ('grails.app.jobs', DEBUG)
logger ('grails.app.services', WARN)
logger ('holo', DEBUG)
logger ('holocore', DEBUG)
logger ('mtcontrolplane', DEBUG)
logger ('capcreator', DEBUG)
logger ('graphqlmt', DEBUG)
logger ('gormfindbylqs', DEBUG)
logger ('grails.artefact.Interceptor', DEBUG)

// logger ('grails.plugin.springsecurity.rest.JwtService', WARN);
// logger ('grails.plugin.springsecurity.rest', DEBUG);
// logger ('grails.plugin.springsecurity', DEBUG);
// logger ('liquibase', DEBUG)
logger ('grails.web.mapping', DEBUG);

// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        charset = StandardCharsets.UTF_8

        pattern =
                '%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} ' + // Date
                        '%clr(%5p) ' + // Log level
                        '%clr(---){faint} %clr([%15.15t]){faint} ' + // Thread
                        '%clr(%-40.40logger{39}){cyan} %clr(:){faint} ' + // Logger
                        '%m%n%wex' // Message
    }
}

def targetDir = BuildSettings.TARGET_DIR
if (Environment.isDevelopmentMode() && targetDir != null) {
    appender("FULL_STACKTRACE", FileAppender) {
        file = "${targetDir}/stacktrace.log"
        append = true
        encoder(PatternLayoutEncoder) {
            charset = StandardCharsets.UTF_8
            pattern = "%level %logger - %msg%n"
        }
    }
    logger("StackTrace", ERROR, ['FULL_STACKTRACE'], false)
}
root(ERROR, ['STDOUT'])
