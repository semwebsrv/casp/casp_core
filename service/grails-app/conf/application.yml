---
server:
    servlet:
        context-path: /caspcore
grails:
    profile: com.k-int.grails.profiles:mtapi:1.0.0-SNAPSHOT
    codegen:
        defaultPackage: capcreator
    cors:
        enabled: true
    gorm:
        reactor:
            # Whether to translate GORM events into Reactor events
            # Disabled by default for performance reasons
            events: false
        # mtcp provides us with an interceptor that will map X-TENANT onto the required header
        multiTenancy:
            # See https://guides.grails.org/database-per-tenant/guide/index.html
            #     https://guides.grails.org/grails-dynamic-multiple-datasources/guide/index.html
            mode: DATABASE
            # Sesssion Tenant Resolver looks for a session attribute called gorm.tenantId
            # It is probably easiest to use an interceptor to set this from whatever local mechanism
            # you use - for example - a HTTP Header
            tenantResolverClass: org.grails.datastore.mapping.multitenancy.web.SessionTenantResolver
    plugin:
        springsecurity:
            userLookup:
                userDomainClassName: 'holo.core.user.HoloUser'
                authorityJoinClassName: 'holo.core.user.HoloUserHoloRole'
                # Identifies the name from the principle that contains the username to lookup
                # See https://github.com/grails-plugins/grails-spring-security-core/blob/master/plugin/grails-app/services/grails/plugin/springsecurity/SpringSecurityService.groovy
                # line 85
                usernamePropertyName: 'username'
            authority:
                className: 'holo.core.user.HoloRole'
            providerNames:
                # holoAuthenticationProvider is defined inside mtcontrolplane/MTControlPlaneGrailsPlugin and is declared in doWithSpring
                - 'holoAuthenticationProvider'
            controllerAnnotations:
              staticRules:
                  -
                      pattern: '/application/**'
                      access:
                          - 'permitAll'
                  -
                      pattern: '/**'
                      access:
                          - 'permitAll'
                  -
                      pattern: '/graphql'
                      access:
                          - 'ROLE_ccuser'
            filterChain:
                chainMap:
                    -
                        pattern: '/**'
                        filters: 'JOINED_FILTERS,-securityContextPersistenceFilter,-rememberMeAuthenticationFilter,-basicAuthenticationFilter'
        databinding:
            dateFormats:
                - "yyyy-MM-dd'T'HH:mm:ssX"
                - "yyyy-MM-dd'T'HH:mm:ss.SSSX"
                - "yyyy-MM-dd'T'HH:mm:ss'Z'"
                - "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                - "yyyy-MM-dd'T'HH:mm:ss"
                - "yyyy-MM-dd'T'HH:mm:ss.SSS"
                - 'yyyy-MM-dd'
info:
    app:
        name: caspcore
        version: '@info.app.version@'
        grailsVersion: '@info.app.grailsVersion@'
spring:
    jmx:
        unique-names: true
    main:
        banner-mode: "off"
    groovy:
        template:
            check-template-location: false
    devtools:
        restart:
            additional-exclude:
                - '*.gsp'
                - '**/*.gsp'
                - '*.gson'
                - '**/*.gson'
                - 'logback.groovy'
                - '*.properties'
management:
    endpoints:
        enabled-by-default: false

---
grails:
    mime:
        disable:
            accept:
                header:
                    userAgents:
                        - Gecko
                        - WebKit
                        - Presto
                        - Trident
        types:
            json:
              - application/json
              - text/json   
            hal:
              - application/hal+json
              - application/hal+xml  
            xml:
              - text/xml
              - application/xml                                 
            atom: application/atom+xml
            css: text/css
            csv: text/csv
            js: text/javascript
            rss: application/rss+xml
            text: text/plain
            all: '*/*'            
    urlmapping:
        cache:
            maxsize: 1000
    controllers:
        defaultScope: singleton
    converters:
        encoding: UTF-8
    cors:
        enabled: true
---
hibernate:
    cache:
        queries: false
        use_second_level_cache: false
        use_query_cache: false
dataSource:
    pooled: true
    jmxExport: true
    driverClassName: org.h2.Driver
    username: sa
    password: ''
environments:
    development:
        keycloakUrl: 'http://localhost:8081'
        dataSource:
            dbCreate: none
            url: jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE
        dataSources:
            mtcp:
                dbCreate: none
                driverClassName: org.postgresql.Driver
                username: 'mtcp'
                password: 'mtcp'
                url: 'jdbc:postgresql://localhost:5432/mtcp_registry'
                dialect: org.hibernate.dialect.PostgreSQL94Dialect
    test:
        keycloakUrl: 'http://localhost:8081'
        dataSource:
            dbCreate: none
            url: jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE
        dataSources:
            mtcp:
                dbCreate: none
                driverClassName: org.postgresql.Driver
                username: 'mtcp'
                password: 'mtcp'
                url: 'jdbc:postgresql://localhost:5432/mtcp_registry'
                dialect: org.hibernate.dialect.PostgreSQL94Dialect
    production:
        keycloakUrl: "${KEYCLOAK_URL}"
        dataSource:
            dbCreate: none
            url: jdbc:h2:./prodDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE
            properties:
                jmxEnabled: true
                initialSize: 5
                maxActive: 50
                minIdle: 5
                maxIdle: 25
                maxWait: 10000
                maxAge: 600000
                timeBetweenEvictionRunsMillis: 5000
                minEvictableIdleTimeMillis: 60000
                validationQuery: SELECT 1
                validationQueryTimeout: 3
                validationInterval: 15000
                testOnBorrow: true
                testWhileIdle: true
                testOnReturn: false
                jdbcInterceptors: ConnectionState
                defaultTransactionIsolation: 2 # TRANSACTION_READ_COMMITTED
        dataSources:
            mtcp:
                dbCreate: none
                driverClassName: org.postgresql.Driver
                username: "${DATASOURCES_MTCP_USERNAME}"
                password: "${DATASOURCES_MTCP_PASSWORD}"
                url: "${DATASOURCES_MTCP_URL}"
                dialect: org.hibernate.dialect.PostgreSQL94Dialect
---
# This setting is used by MTJwtAuthenticationFilter.groovy and provides the connection to keycloak to validate JWTs
