package capcreator

import grails.core.GrailsApplication
import grails.plugins.*
import grails.rest.*
import grails.converters.*

class ApplicationController implements PluginManagerAware {

    GrailsApplication grailsApplication
    GrailsPluginManager pluginManager

    def index() {
        // [grailsApplication: grailsApplication, pluginManager: pluginManager]
        Map result = [
          'app': 'casp_core'
        ]
        render result as JSON
    }
}
