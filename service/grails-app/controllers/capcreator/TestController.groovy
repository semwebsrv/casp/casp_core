package capcreator


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import grails.gorm.multitenancy.CurrentTenant

@CurrentTenant
class TestController {

  def springSecurityService
  // static responseFormats = ['json', 'xml']
	
  @Secured('ROLE_ccuser')
  def index() { 
    String tenant = request.getHeader('X-TENANT')
    def ath = springSecurityService.authentication
    def pri = springSecurityService.principal

    def usr = null; 
    try {
      usr = springSecurityService.currentUser
    }
    catch ( Exception e ) {
      log.error("Problem resolving user",e);
    }

    log.debug("*****test::index() tenant=${tenant} user=${usr} auth=${ath} usercls=${usr?.class?.name} authcls=${ath?.class?.name}");
    def result = [ status: 'OK' ]
    render result as JSON
  }
}
