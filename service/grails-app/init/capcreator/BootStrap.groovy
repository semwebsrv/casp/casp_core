package capcreator

class BootStrap {

  def grailsApplication

  def init = { servletContext ->
    log.info("**casp-core-service** ${grailsApplication.metadata.'info.app.version'} / ${grailsApplication.metadata.'build.time'}");
    // log.info("HoloUser graphql config: ${holo.core.user.HoloUser.query_config}");
    log.info("MTCP URL: ${grailsApplication.config?.dataSources?.mtcp?.url}");
    log.info("Keycloak URL: ${grailsApplication.config.keycloakUrl}");
  }

  def destroy = {
  }

}
