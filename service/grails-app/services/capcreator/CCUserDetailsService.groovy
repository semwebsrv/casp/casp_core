package capcreator

import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUser
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import holo.core.user.HoloUser;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.context.SecurityContextHolder;

import org.grails.web.util.WebUtils;
import javax.servlet.http.HttpServletRequest;
import grails.gorm.multitenancy.CurrentTenant
import capcreator.CCUser

@Transactional
class CCUserDetailsService implements GrailsUserDetailsService {

   /**
    * Some Spring Security classes (e.g. RoleHierarchyVoter) expect at least
    * one role, so we give a user with no granted roles this one which gets
    * past that restriction but doesn't grant anything.
    */
   static final List NO_ROLES = [new SimpleGrantedAuthority(SpringSecurityUtils.NO_ROLE)]
   static final List SYSTEM_ROLES = [new SimpleGrantedAuthority('ROLE_SYSTEM')]

   UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException {
      return loadUserByUsername(username)
   }

   @CurrentTenant
   @Transactional(readOnly=true, noRollbackFor=[IllegalArgumentException, UsernameNotFoundException])
   UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

      log.debug("CCUserDetailsService::loadUserByUsername ${username}");
      UserDetails result = null;

      log.debug("CCUserDetailsService::loadUserByUsername(${username})");
      log.debug("authn: ${SecurityContextHolder.context.getAuthentication()}");

      try {
        HttpServletRequest http_req = WebUtils.retrieveGrailsWebRequest().getCurrentRequest()
        log.debug("http request:: req:${http_req}/username:${username}/header:${http_req.getHeader('Authorization')}/headernames:${http_req.getHeaderNames()}");
  
        String tenant = http_req.getHeader('X-TENANT') 
        if ( tenant != null ) {
          log.debug("Using standard tenant ${tenant}");
  
          // def ip = request.getRemoteAddr()
          HoloUser user = HoloUser.findByUsername(username)
          if (user == null) {
            log.debug("Unable to locate user ${username} in tenant ${tenant}");
            throw new UsernameNotFoundException("User not found ${username}".toString())
          }
       
          def authorities = user.authorities.collect {
            new SimpleGrantedAuthority(it.authority)
          }
      
          result = new CCUser(user.username, 
                              user.password, 
                              user.enabled, 
                              !user.accountExpired, 
                              !user.passwordExpired, 
                              !user.accountLocked, 
                              authorities ?: NO_ROLES,
                              tenant, 
                              user.id)
        }
        else {
          log.debug("\n\n******  Tennantless authentication - must be internal control plane ******\n\n");
          
          if ( username == 'admin' ) {
            result = new User('admin', '', true, true, true, true, SYSTEM_ROLES);
          }
          else { 
            throw new RuntimeException("unhandled __SYSTEM user");
          }
        }
      }
      catch ( Exception e ) {
        log.error("Problem processing user",e);
      }

      log.debug("Returning result: ${result}");
      return result;
   }
}
