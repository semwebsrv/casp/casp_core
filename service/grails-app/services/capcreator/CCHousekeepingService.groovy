package capcreator

import grails.gorm.transactions.Transactional
import mtcontrolplane.InstallOrUpgradeTenantEvent
import mtcontrolplane.NewAuthTenantEvent
import org.springframework.context.event.EventListener
import holo.core.user.*;
import grails.gorm.multitenancy.Tenants
import grails.events.annotation.gorm.Listener
import org.grails.datastore.mapping.engine.event.PreInsertEvent
import org.grails.datastore.mapping.engine.event.SaveOrUpdateEvent 
import org.grails.datastore.mapping.engine.event.AbstractPersistenceEvent 
import org.springframework.context.ApplicationListener
import capcreator.AlertBase
import capcreator.Widget
import holo.core.parties.Party;
import grails.gorm.multitenancy.CurrentTenant
import holo.core.grant.Grant;


// N.B. Removed @transactional annotation - we want control over the transactions in here
@CurrentTenant
class CCHousekeepingService {

  @EventListener
  public void onInstallOrUpgradeTenant(InstallOrUpgradeTenantEvent event) {
    log.info("CCHousekeepingService::onInstallOrUpgradeTenant(${event}) tenant=${Tenants.currentId()}");
    HoloUser.withTransaction { status ->

      log.info("inside transaction block(${event}) tenant=${Tenants.currentId()}");

      if ( ( event.params != null ) &&
           ( event.params.rootOU != null ) ) {
        Party p = Party.findByShortcode(event.params.rootOU.code) ?: new Party(shortcode:event.params.rootOU.code, name:event.params.rootOU.name).save(flush:true, failOnError:true);
        log.debug("New party ID : ${p.getId()}");
      }

      if ( ( event.params != null ) &&
           ( event.params.admuser != null ) ) {
        // admuser:[username:'admin', password:'admin']
        log.debug("Process admin user for tenant: ${event.params.admuser}");
        HoloRole role_adm = HoloRole.findByAuthority('AdminRole') ?: new HoloRole(authority:'AdminRole').save(flush:true, failOnError:true);
        HoloRole role_system = HoloRole.findByAuthority('ROLE_SYSTEM') ?: new HoloRole(authority:'ROLE_SYSTEM').save(flush:true, failOnError:true);
        HoloRole role_user = HoloRole.findByAuthority('ROLE_USER') ?: new HoloRole(authority:'ROLE_USER').save(flush:true, failOnError:true);
  
        HoloUser admUser = HoloUser.findByUsername(event.params.admuser.username) 
        if ( admUser == null ) {
          log.debug("Create new user");
          admUser = new HoloUser(username:event.params.admuser.username,
                                 password:'*',
                                 enabled:true,
                                 accountExpired:false,
                                 accountLocked:false,
                                 passwordExpired:false).save(flush:true, failOnError:true);
          HoloUserHoloRole hur1 = new HoloUserHoloRole(holoUser:admUser, holoRole:role_adm).save(flush:true, failOnError:true);
          HoloUserHoloRole hur2 = new HoloUserHoloRole(holoUser:admUser, holoRole:role_system).save(flush:true, failOnError:true);
          HoloUserHoloRole hur3 = new HoloUserHoloRole(holoUser:admUser, holoRole:role_user).save(flush:true, failOnError:true);
        }
        else {
          log.debug("Found existing user");
        }

        // The admin user gets to do everything to everything over all partitions / parties / subtenants
        log.debug("Create new grant");
        Grant g = new Grant(
                         grantResourceOwner:'%',
                          grantResourceType:'%',
                            grantResourceId:'%',
                                grantedPerm:'%',
                                granteeType:'USER',
                                  granteeId:admUser.username).save(flush:true, failOnError:true);
      }
      else {
        log.info("onInstallOrUpgradeTenant - event had no params / admUser : ${event.params}");
      }

    }

  }

   private void onNoEvent(NewAuthTenantEvent event) {

     HoloUser user = HoloUser.findByUsername(event.username)
     if ( user == null ) {
       log.debug("Create new user");
       HoloUser.withNewTransaction { status ->
         user = new HoloUser(username:event.username,
                             password:'notused',
                             enabled:true,
                             accountExpired:false,
                             accountLocked:false,
                             passwordExpired:false).save(flush:true, failOnError:true);
         log.debug("Create new user for ${event.username}");
       }
     }
     else {
       log.debug("Found existing user");
     }

   }

  // @EventListener
  // onPreSaveInsertEvent(PreInsertEvent event) {
  //   log.debug("CCHousekeepingService::HoloUser new pre save event");
  // }

  // @Listener
  // void onPreInsertEvent(PreInsertEvent event) {
  //   log.debug("CCHousekeepingService::HoloUser ${event}");
  // }

  // Seems that @Listener methods aren't being called in a MT context, but this does work::
  // public void onApplicationEvent(org.springframework.context.ApplicationEvent event){
  //   log.debug("onApplicationEvent.... ${event}");
  // }

}
