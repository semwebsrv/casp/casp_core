import groovy.sql.Sql

String MODULE_ID="casp_core"


// Set your module ID string
databaseChangeLog = {
  include file: 'extensions.groovy', relativeToChangelogFile:true
  include file: 'mcontrolplane/changelog.groovy', relativeToChangelogFile:true
  include file: 'holocore/changelog.groovy', relativeToChangelogFile:true
  include file: 'capcreator/changelog_1_0_0.groovy', relativeToChangelogFile:true

  // At the end of a given release changes, update the mtcp_version_info entry for this module
  changeSet(author: "ianibbo (manual)", id: "${MODULE_ID}_v0_1_ver") {
    grailsChange {
      change {
        // Default all values currently existing to the correct License class.
        if ( sql.rows("select * from mtcp_version_info where vi_module_id='${MODULE_ID}'").size() == 0 )  {
          sql.execute("insert into mtcp_version_info(vi_id,vi_ver,vi_module_id,vi_module_version,vi_install_date) values (?,?,?,?,?)",
                      [java.util.UUID.randomUUID().toString(),0,MODULE_ID,'0.1',new Date().toString()]);
        }
        else {
          sql.executeUpdate("update mtcp_version_info set vi_module_version='1.0.0-SNAPSHOT',vi_install_date='${new Date().toString()}' where vi_module_id='${MODULE_ID}'")
        }
      }
    }
  }

}

