package capcreator;

import org.springframework.security.core.userdetails.User
import org.springframework.security.core.GrantedAuthority

public class CCUser extends User {

  String tenant;
  String id;

  public CCUser(java.lang.String username, 
                java.lang.String password, 
                boolean enabled, 
                boolean accountNonExpired, 
                boolean credentialsNonExpired, 
                boolean accountNonLocked, 
                java.util.Collection<? extends GrantedAuthority> authorities,
                String tenant,
                String id) {
    super(username,password,enabled,accountNonExpired,credentialsNonExpired,accountNonLocked,authorities);
    this.tenant = tenant;
    this.id = id;
  }

  public String toString() {
    return "${super.toString()} ${tenant} ${id}".toString()
  }
}
