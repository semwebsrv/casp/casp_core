package capcreator

import graphqlmt.GraphqlContextBuilder;
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.springframework.beans.factory.annotation.Autowired
import grails.plugin.springsecurity.SpringSecurityService

public class CAPGraphqlContextBuilder implements GraphqlContextBuilder {

  @Autowired
  SpringSecurityService springSecurityService

  Map buildContext(GrailsWebRequest request) {
    Map result = [
      textContextProperity:'testContextValue',
      springSecurityService:springSecurityService
    ]
  }
}

