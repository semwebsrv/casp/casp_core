
CREATE USER ccdev WITH PASSWORD 'ccdev';
DROP DATABASE if exists ccdev01;
CREATE DATABASE ccdev01;
GRANT ALL PRIVILEGES ON DATABASE ccdev01 to ccdev;

\c mtcp_registry

insert into mtcp_dsinfo values 
( 'CCDEV01', 0, 'CCDEV01', 'jdbc:postgresql://localhost:5432/ccdev01', 'ccdev','ccdev', 'org.postgresql.Driver', 'org.hibernate.dialect.PostgreSQL94Dialect', 'none' );
    
insert into mtcp_tenant values 
( 'DemoTenant01', 0, 'DemoTenant01' );
    
insert into mtcp_service values
( 'capcreator', 0, 'capcreator' );
    
insert into mtcp_service_version values 
( 'capcreator_0_1', 0, 'capcreator', '0.1' );
    
insert into mtcp_tenant_service_version values
( 'DT01CC01', 0, 'capcreator_0_1', 'DemoTenant01', 'CCDEV01');

