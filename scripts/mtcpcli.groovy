import groovy.grape.Grape

@GrabResolver(name='mvnRepository', root='http://central.maven.org/maven2/')
@GrabResolver(name='kint', root='http://nexus.k-int.com/content/repositories/releases')
@Grab('io.github.http-builder-ng:http-builder-ng-core:1.0.4')
@Grab('commons-codec:commons-codec:1.14')
@Grab('org.ini4j:ini4j:0.5.4')
@Grab('io.jsonwebtoken:jjwt-impl:0.11.0')
@Grab('io.jsonwebtoken:jjwt-jackson:0.11.0')


// Normally run with
// groovy ./mtcpcli.groovy


import org.ini4j.*;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import static groovyx.net.http.HttpBuilder.configure
import io.jsonwebtoken.Claims;
import javax.xml.bind.DatatypeConverter;
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec

private createGroupGraphql(groovyx.net.http.HttpBuilder http, 
                           String jwt, 
                           String tenant, 
                           String groupName) {


  def record = [
    'groupName':groupName
  ]

  def result = http.post {
    request.uri.path = '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/json'
    request.headers['X-TENANT'] = tenant
    request.contentType='application/json'
    request.headers['Authorization'] = "Bearer $jwt".toString()
    request.body =  [
          "query" : 'mutation($group: HoloGroupInputType) { createHoloGroup(hologroup: $group) { id groupName } }',
          "variables": [
            "group" : record
          ]
        ]
    response.when(200) { FromServer fs, Object body ->
      println("graphql create group returns 200 ${body}");
      println("${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      System.exit(1);
    }
  }
}

private createGrantGraphql(groovyx.net.http.HttpBuilder http, 
                           String jwt, 
                           String tenant, 
                           String grantResourceOwner,
                           String grantResourceType,
                           String grantResourceId,
                           String grantedPerm,
                           String granteeType,
                           String granteeId) {


  def record = [
    'grantResourceOwner':grantResourceOwner,
    'grantResourceType':grantResourceType,
    'grantResourceId':grantResourceId,
    'grantedPerm': grantedPerm,
    'granteeType':granteeType,
    'granteeId':granteeId
  ]

  def result = http.post {
    request.uri.path = '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/json'
    request.headers['X-TENANT'] = tenant
    request.contentType='application/json'
    request.headers['Authorization'] = "Bearer $jwt".toString()
    request.body =  [
          "query" : 'mutation($grant: GrantInputType) { createGrant(grant: $grant) { id } }',
          "variables": [
            "grant" : record
          ]
        ]
    response.when(200) { FromServer fs, Object body ->
      println("graphql createGrantGraphql returns 200 ${body}");
      println("${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      System.exit(1);
    }
  }
}

private String getSchema(groovyx.net.http.HttpBuilder http, String jwt, String tenant) {

  String result = null;
  def postres = http.post {
    request.uri.path = '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/json'
    request.headers['X-TENANT'] = tenant
    request.contentType='application/json'
    request.headers['Authorization'] = "Bearer $jwt".toString()
    request.body =  [
      "query" : '{ __schema { types { name } } }'
    ]


    response.when(200) { FromServer fs, Object body ->
      println("graphql query returns 200 ${body}");
      println("${body}");
      result = body;
    }

    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      System.exit(1);
    }
  }
  return result
}

private createAlertViaGraphql(groovyx.net.http.HttpBuilder http, String jwt, String tenant, String title, String party_id) {

  def record = [
    'title':title,
    'owner': [
      'id':party_id
    ]
  ]

  println("Create alert: ${record}");

  def result = http.post {
    request.uri.path = '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/json'
    request.headers['X-TENANT'] = tenant
    request.contentType='application/json'
    request.headers['Authorization'] = "Bearer $jwt".toString()
    request.body =  [
      "query" : 'mutation($alertbase: AlertBaseInputType) { createAlertBase(alertbase: $alertbase) { id title } }',
      "variables": [
        "alertbase" : record
      ]
    ]

    response.when(200) { FromServer fs, Object body ->
      println("graphql query returns 200 ${body}");
      println("${body}");
    }

    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      System.exit(1);
    }
  }
}


private queryAlertViaGraphql(groovyx.net.http.HttpBuilder http, String jwt, String tenant, String qry) {
  def result = http.post {
    request.uri.path = '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/json'
    request.headers['X-TENANT'] = tenant
    request.contentType='application/json'
    request.headers['Authorization'] = "Bearer $jwt".toString()
    request.body = [
      'query': "query { findAlertBaseByLuceneQuery(luceneQueryString:\"title:${qry}\") { totalCount results { title } } }".toString(),
      'variables':[:]
    ]
    response.when(200) { FromServer fs, Object body ->
      println("graphql query returns 200 ${body}");
      println("${body?.data?.findAlertBaseUsingLQS?.totalCount} ${body?.data?.findAlertBaseUsingLQS?.results?.size}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      System.exit(1);
    }
  }
}

private genericGraphqlQuery(groovyx.net.http.HttpBuilder http, 
                            String jwt, 
                            String tenant, 
                            String type, 
                            String qry, 
                            String props) {
  def result = http.post {
    request.uri.path = '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/json'
    request.headers['X-TENANT'] = tenant
    request.contentType='application/json'
    request.headers['Authorization'] = "Bearer $jwt".toString()
    request.body = [
      'query': "query { find${type}ByLuceneQuery(luceneQueryString:\"${qry}\") { totalCount results { id ${props} } } }".toString(),
      'variables':[:]
    ]

    response.when(200) { FromServer fs, Object body ->
      String propName="find${type}ByLuceneQuery".toString();
      // println("graphql query returns 200 ${body}");
      result = body.data[propName]
    }

    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
      System.exit(1);
    }
  }
  return result;
}


def registerTenant(mt_context, http, jws, tenant, resolvers=[]) {  println("Call ${mt_context.tenant_admin_path+'/installOrUpgrade'}");

  def result = http.post {
    request.uri.path = mt_context.tenant_admin_path+'/registerTenant'
    request.headers['Authorization'] = "Bearer $jws".toString()
    request.headers['accept']='application/json'
    request.contentType='application/json'
    request.body = [
      tenant: tenant,
      resolvers: resolvers
    ]

    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${new String(body)} ${fs} ${fs.getStatusCode()}");
    }
  }
  return result;

}

def installOrUpgrade(mt_context, http, jws, tenant, insbody=[:]) {

  println("Call ${mt_context.tenant_admin_path+'/installOrUpgrade'}");

  def result = http.post {
    request.uri.path = mt_context.tenant_admin_path+'/installOrUpgrade'
    request.headers['Authorization'] = "Bearer $jws".toString()
    request.headers['accept']='application/json'
    request.contentType='application/json'
    request.body = insbody
  
    request.uri.query = [
      tenant:tenant
    ]
    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${new String(body)} ${fs} ${fs.getStatusCode()}");
    }
  }
  return result;
}

def callTest(mt_context, http, jws, tenant) {
  def result = http.get {
    request.uri.path = mt_context.base_path+'/test'
    request.headers['Authorization'] = "Bearer $jws".toString()
    request.headers['X-TENANT'] = tenant
    request.headers['accept']='application/json'
    request.contentType='application/json'
    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }
}


def registerDataSource(mt_context,
                       http, 
                       jws, 
                       dscode, 
                       driver, 
                       user, 
                       pass, 
                       dbcreate, 
                       dbname, 
                       dbhost, 
                       dialect) {
  def result = http.get {
    request.uri.path = mt_context.tenant_admin_path+'/registerDataSource'
    // request.uri.path = '/test'
    request.headers['Authorization'] = "Bearer $jws".toString()
    // request.headers['X-TENANT'] = "__SYSTEM"
    request.headers['accept']='application/json'
    request.contentType='application/json'

    request.body= [ 
      dscode:dscode,
      driverClassName: driver,
      username: user,
      password: pass,
      dbcreate: dbcreate,
      dbname: dbname,
      dbhost: dbhost,
      dialect: dialect
    ]

    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }
  return result;

}

def registerTSVDS(mt_context, http, jws, tenant, service, version, dscode) {
  def result = http.get {
    request.uri.path = mt_context.tenant_admin_path+'/registerTSVDS'

    // request.uri.path = '/test'
    request.headers['Authorization'] = "Bearer $jws".toString()
    // request.headers['X-TENANT'] = "__SYSTEM"
    request.headers['accept']='application/json'
    request.contentType='application/json'

    request.body= [
      dscode:dscode,
      service: service,
      version: version,
      tenant: tenant
    ]

    response.when(200) { FromServer fs, Object body ->
      println("OK ${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }
  return result;

}

def provisionPostgresDatabase(mt_context, http, jws, dbname, user, pass) {

  def result = http.get {
    request.uri.path = mt_context.tenant_admin_path+'/provision'
    // request.uri.path = '/test'
    request.headers['Authorization'] = "Bearer $jws".toString()
    // request.headers['X-TENANT'] = "__SYSTEM"
    request.headers['accept']='application/json'
    request.contentType='application/json'
    request.body=[
      dbname: dbname,
      user: user,
      pass:pass
    ]
    
    response.when(200) { FromServer fs, Object body ->
      println("response body: ${body}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }
  return result;
}

// https://stormpath.com/blog/jwt-java-create-verify
// https://stackoverflow.com/questions/37722090/java-jwt-with-public-private-keys
// https://github.com/jwtk/jjwt/issues/131
//
def validate(String jwt, String publickey_str) {

  byte[] publicBytes = Base64.getDecoder().decode(publickey_str);
  X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
  KeyFactory keyFactory = KeyFactory.getInstance("RSA");
  PublicKey key = keyFactory.generatePublic(keySpec);

  // PublicKey publicKey = decodePublicKey(pemToDer(publickey));
  Claims claims = Jwts.parser()         
       .setSigningKey(key)
       .parseClaimsJws(jwt).getBody();

  return claims
}

def getPublicKey(HttpBuilder keycloak, String realm) {
  String result = null;
  def http_res = keycloak.get {
    request.uri.path = "/auth/realms/${realm}".toString()

    response.when(200) { FromServer fs, Object body ->
      // println("OK ${body}");
      result = body.public_key
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }

  return result;
}

def login(HttpBuilder keycloak, String client, String realm, String username, String password) {

  String result = null;

  def http_res = keycloak.post {
    request.uri.path = "/auth/realms/${realm}/protocol/openid-connect/token".toString()
    request.contentType='application/x-www-form-urlencoded'
    request.body=[
      client_id:client,
      username:username,
      password:password,
      grant_type:'password'
    ]

    response.when(200) { FromServer fs, Object body ->
      // println("OK ${body}");
      result = body.access_token
    }
    response.failure { FromServer fs, Object body ->
      println("Problem ${body} ${fs} ${fs.getStatusCode()}");
    }
  }
  return result;

}

def createKeycloakRealm(Map mt_context, String realm) {

  def r2 = mt_context.keycloak.post {
    request.uri.path = "/auth/admin/realms"
    request.headers['Authorization'] = "bearer ${mt_context.keycloak_adm_jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.body = [ 
      realm:realm,
      enabled:true,
      clients:[
        [
          name:'holo',
          clientId:'holo',
          standardFlowEnabled:true,
          implicitFlowEnabled:true,
          directAccessGrantsEnabled:true,
          webOrigins:[
            '*'
          ],
          redirectUris:[
            'http://localhost:4000'
          ],
          publicClient: true,
          clientAuthenticatorType: "client-secret"
        ]
      ]
    ]
    response.success { FromServer fs, Object body ->
      println("Realm ${realm} created body:${body} code:${fs?.getStatusCode()}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating realm ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }

}


def createKeycloakRole(Map mt_context, String realm, String role) {
  println("Create role ${role} in realm ${realm}");
  def r2 = mt_context.keycloak.post {
    request.uri.path = "/auth/admin/realms/${realm}/roles"
    request.headers['Authorization'] = "bearer ${mt_context.keycloak_adm_jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.body = [
      name:role
    ]
    response.success { FromServer fs, Object body ->
      println("Role ${role} created in realm ${realm} created body:${body} code:${fs?.getStatusCode()}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating role ${role} ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }

}


// bug https://issues.redhat.com/browse/KEYCLOAK-5038
// realmRoles ignored
def createKeycloakGroup(Map mt_context, HttpBuilder keycloak, String jwt, String realm, String group, List<String> roles) {
  println("Create group ${group} in realm ${realm} with roles ${roles}");
  def r2 = keycloak.post {
    request.uri.path = "/auth/admin/realms/${realm}/groups"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.body = [
      name:group,
      realmRoles:roles
    ]
    response.success { FromServer fs, Object body ->
      println("Group ${group} ${realm} created body:${body} code:${fs?.getStatusCode()}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem in create group  ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }
}

def getGroup(HttpBuilder keycloak, String jwt, String realm, String group) {
  def result = null;
  def r2 = keycloak.get {
    request.uri.path = "/auth/admin/realms/${realm}/groups"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.uri.query=[
      search:group
    ]
    response.success { FromServer fs, Object body ->
      println("Group get group ${group} in realm ${realm} returned body:${body} code:${fs?.getStatusCode()}");
      if ( body.size() == 1 )
      result = body[0];
    }
    response.failure { FromServer fs, Object body ->
      println("Problem in get group  ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }
  return result;
}

Map getRole(HttpBuilder keycloak, String jwt, String realm, String role) {
  Map result = null;
  def r1 = keycloak.get {
    request.uri.path = "/auth/admin/realms/${realm}/roles/${role}"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    response.success { FromServer fs, Object body ->
      println("Realm ${realm} lookup role ${role} returns body:${body} code:${fs?.getStatusCode()}");
      result = body
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating realm ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }
  return result;
}

def updateGroup(HttpBuilder keycloak, String jwt, String realm, String group_id, List<String> roles) {
  println("updateGroup(...${realm},${group_id},${roles})");

  if ( group_id != null ) {
    println("update group ${group_id} in realm ${realm} with roles ${roles}");
    def r2 = keycloak.put {
      request.uri.path = "/auth/admin/realms/${realm}/groups/${group_id}"
      request.headers['Authorization'] = "bearer ${jwt}"
      request.headers['accept'] = 'application/json'
      request.contentType='application/json'
      request.body = [
        realmRoles:roles
      ]
      response.success { FromServer fs, Object body ->
        println("Realm ${realm} created body:${body} code:${fs?.getStatusCode()}");
      }
      response.failure { FromServer fs, Object body ->
        println("Problem creating realm ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
      }
    }
  }
  else {
    println("Unable to locate group_id. Not updating");
  }
}

def createRealmMapping(HttpBuilder keycloak, String jwt, String realm, String group_id, String role_name) {

  def role_info = getRole(keycloak,jwt,realm,role_name)
  println("  -> Attempting to add role with role ${role_info} to group ${group_id} in realm ${realm}");

  def r2 = keycloak.post {
    request.uri.path = "/auth/admin/realms/${realm}/groups/${group_id}/role-mappings/realm"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.body = [
      role_info
    ]
    response.success { FromServer fs, Object body ->
      println("Create Realm Mapping ${realm} created body:${body} code:${fs?.getStatusCode()}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating realm role mapping ${realm} ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }
}


def createKeycloakUser(HttpBuilder keycloak, 
                       String jwt, 
                       String realm, 
                       String username,
                       String password,
                       String firstName,
                       String lastName,
                       String email,
                       List groups,
                       List roles) {
  String user_id = null;

  // Create the user
  def r2 = keycloak.post {
    request.uri.path = "/auth/admin/realms/${realm}/users"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.body = [
      username:username,
      email:email,
      emailVerified: true,
      firstName:firstName,
      lastName:lastName,
      groups: groups,
      enabled:true,
      realmRoles: roles
    ]
    response.success { FromServer fs, Object body ->
      println("${username} created in realm ${realm} body:${body} code:${fs?.getStatusCode()}");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating user ${username} in realm ${realm} - ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }

  // Lookup the user
  def r3 = keycloak.get {
    request.uri.path = "/auth/admin/realms/${realm}/users"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.uri.query = [
      username:username,
    ]
    response.success { FromServer fs, Object body ->
      println("Lookup user returns ${body}");
      user_id = body[0]?.id;
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating admin user ${body} ${fs} ${fs.getStatusCode()}");
    }
  }

  // Set the password
  def r4 = keycloak.put {
    request.uri.path = "/auth/admin/realms/${realm}/users/${user_id}/reset-password"
    request.headers['Authorization'] = "bearer ${jwt}"
    request.headers['accept'] = 'application/json'
    request.contentType='application/json'
    request.body = [
      value:password,
      temporary:false
    ]
    response.when(200) { FromServer fs, Object body ->
      println("Password set");
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating admin user ${body} ${fs} ${fs.getStatusCode()}");
    }
  }


}

private Map createPartyGraphQL(groovyx.net.http.HttpBuilder http, 
                               String jwt, 
                               String tenant, 
                               String name, 
                               String shortcode, 
                               String parent_id){

  def result = null;

  println("createPartyGraphQL(..., ${tenant}, ${name}, ${shortcode}, ${parent_id})");

  def record = [
    name:name,
    shortcode: shortcode,
    parent: ( ( parent_id != null ) ? [ id: parent_id ] : null )
  ]

  println("Graphql create party request: ${record}");

  http.post { 
    request.uri.path= '/caspcore/graphql'
    request.headers.'accept'='application/json'
    request.headers.'Content-Type'='application/graphql'
    request.headers.'X-TENANT'=tenant
    request.headers.'Authorization'="Bearer ${jwt}".toString()
    request.headers.'Content-Type'='application/json'
    request.contentType='application/json'
    request.body =  [
          "query" : 'mutation($party: PartyInputType) { createParty(party: $party) { id name shortcode parent { id name } } }',
          "variables": [
            "party" : record
          ]
        ]

    response.success { FromServer fs, Object body ->
      println("Party created body:${body} code:${fs?.getStatusCode()}");
      result = body.data.createParty;
    }
    response.failure { FromServer fs, Object body ->
      println("Problem creating party ${body} ${fs} ${fs?.getStatusCode()}");
    }
  }

  println("createPartyGRAPHQL returning with ${result}");
  return result;
}

def provisionTenantInKeycloak(mt_context,
                              new_tenant, 
                              keycloak,
                              keycloak_adm_jwt){

  // We're going to create a new tenant called alerthub
  println("Provision keycloak realm for new tenant ${new_tenant}")
  createKeycloakRealm(mt_context, new_tenant)
  
  // Create an admin and a user group in the new realm
  // Create realm level roles
  println("Creating roles");
  createKeycloakRole(mt_context, new_tenant, 'ccadmin')
  createKeycloakRole(mt_context, new_tenant, 'ccuser')
  createKeycloakRole(mt_context, new_tenant, 'scc_user_role')
  
  // println("Looking up roles");
  // String ccadmin_role_id = getRoleId(keycloak, keycloak_adm_jwt, new_tenant, 'ccadmin')
  // String ccuser_role_id = getRoleId(keycloak, keycloak_adm_jwt, new_tenant, 'ccuser')

  println("Creating groups");
  // Create groups - we need the groups to contain the roles above
  createKeycloakGroup(mt_context, keycloak, keycloak_adm_jwt, new_tenant, 'ccadmin', ['ccadmin', 'ccuser' ] )
  createKeycloakGroup(mt_context, keycloak, keycloak_adm_jwt, new_tenant, 'ccuser', ['ccuser'])
  createKeycloakGroup(mt_context, keycloak, keycloak_adm_jwt, new_tenant, 'scc_users', ['scc_user_role'])

  def admin_group_info = getGroup(keycloak, keycloak_adm_jwt, new_tenant, 'ccadmin')
  def user_group_info = getGroup(keycloak, keycloak_adm_jwt, new_tenant, 'ccuser')
  def scc_user_group_info = getGroup(keycloak, keycloak_adm_jwt, new_tenant, 'scc_users')

  // https://stackoverflow.com/questions/48617998/keycloak-rest-api-how-to-map-a-role-with-a-group
  println("Create realm role mappings")
  createRealmMapping(keycloak, keycloak_adm_jwt, new_tenant, user_group_info.id, 'ccuser')
  createRealmMapping(keycloak, keycloak_adm_jwt, new_tenant, admin_group_info.id, 'ccuser')
  createRealmMapping(keycloak, keycloak_adm_jwt, new_tenant, scc_user_group_info.id, 'scc_users')

  // In our hypothetical setup we have an admin user that shouldn't normally be used, and Jana who is the boss and Fred who is a worker
  // Jana will be allowed to see everything, Fred won't

  println("Provision admin user in realm ${new_tenant}");
  createKeycloakUser(keycloak, keycloak_adm_jwt, new_tenant, 'admin', 'password', 'admin', 'admin', 'admin@a.b.v', [ 'ccadmin', 'ccuser' ], []);
  createKeycloakUser(keycloak, keycloak_adm_jwt, new_tenant, 'jana', 'password', 'Jana', 'Jones', 'jana@a.b.v', [ 'ccadmin', 'ccuser', 'scc_users' ], []);
  createKeycloakUser(keycloak, keycloak_adm_jwt, new_tenant, 'fred', 'password', 'fred', 'Smith', 'fred@a.b.v', [ 'ccuser' ], []);
}



/**********************************/
/*              START             */
/**********************************/
//https://github.com/jwtk/jjwt
println("CASP Config");

def cli = new CliBuilder(usage: 'mtcpcli.groovy')

// Create the list of options.
cli.with {
  h longOpt: 'help', 'Show usage information'
  c longOpt: 'config', args: 1, argName: 'config', 'config name'
}
     
def options = cli.parse(args)
if (!options) {
  return
}

String cfgname=options.c ?: 'default'

Wini ini = new Wini(new File(System.getProperty("user.home")+'/.holo/credentials'));

String keycloak_url = ini.get(cfgname, 'keycloak_url', String.class);
String casp_service_url = ini.get(cfgname, 'casp_service_url', String.class);
String kc_clusteradmin_user = ini.get(cfgname, 'clusteradmin_user', String.class);
String kc_clusteradmin_pass = ini.get(cfgname, 'clusteradmin_pass', String.class);
String kc_master_user = ini.get(cfgname, 'kc_master_user', String.class);
String kc_master_pass = ini.get(cfgname, 'kc_master_pass', String.class);

println "mtcpcli.groovy - with config ${cfgname} - keycloak at ${keycloak_url} casp at ${casp_service_url}"
if ( ( cfgname == null ) ||
     ( keycloak_url == null ) ||
     ( casp_service_url == null ) ||
     ( keycloak_url.length() == 0 ) ||
     ( casp_service_url.length() == 0 ) ) {
  println("Invalid configuration");
  System.exit(1);
}

def keycloak = configure {
  request.uri = keycloak_url ?: 'http://localhost:8081'
}

def mtservice = configure {
  request.uri = casp_service_url ?: 'http://localhost:8080'
}


// Grab the keycloak clusteradmin public key

// Log in to clusteradmin user
String publickey = getPublicKey(keycloak, 'clusteradmin')
println("Got public key ${publickey}");

// Step 1 - we need to logon as the clusteradmin realm and user who will have permission to
// set up new data sources



def keycloak_adm_jwt = login(keycloak, 'admin-cli', 'master', kc_master_user, kc_master_pass)
println("keycloak adm jwt: ${keycloak_adm_jwt}");
if ( keycloak_adm_jwt == null ) {
  println("Unable to login as a user with keycloak master perms. Did you forget to provision a master admin user at ${keycloak_url}");
  System.exit(1);
}

// Creating a clusteradmin user with permission to control the cluster
// Not needed once done
// HttpBuilder keycloak, String jwt, String realm, String username, String password, String firstName, String lastName, String email, List groups, List roles

createKeycloakUser(keycloak, keycloak_adm_jwt, 'clusteradmin', kc_clusteradmin_user, kc_clusteradmin_pass, 'admin_first', 'admin_last', 'admin@a.b.v',  [ 'clusteradmin' ], []);

println("Attempt login to clusteradmin realm with ${kc_clusteradmin_user}/${kc_clusteradmin_pass}");

def clusteradmin_jwt = login(keycloak, 'holo', 'clusteradmin', kc_clusteradmin_user, kc_clusteradmin_pass)
if ( clusteradmin_jwt == null ) {
  println("Unable to login as a user with clusteradmin perms. Did you forget to provision a user at ${keycloak_url}");
  System.exit(1);
}

Claims clusteradmin_claims = validate(clusteradmin_jwt, publickey);
println("Claims: ${clusteradmin_claims}");


Map mt_context = [
  keycloak: keycloak,
  keycloak_adm_jwt: keycloak_adm_jwt,
  clusteradmin_jwt: clusteradmin_jwt,
  base_path: '/caspcore',
  tenant_admin_path: '/caspcore/tenantAdmin'
]

String new_tenant='alerthub'
// We're going to create a new tenant called alerthub
println("Provision keycloak realm for new tenant ${new_tenant}")

// Provision a keycloak realm for the new tenant
provisionTenantInKeycloak(mt_context, new_tenant, keycloak, keycloak_adm_jwt);

// Provision a datasource - actually create a database - the provision service is a helper that will create
// a database using the default postgres instance attached to the service. It is entirely possible to manually
// create instances and then register them - this may be necessary if you want to provision over many different
// postgres instances. This step does not register any information about the new postgres database - its


// provisionCaspServiceForTenant(new_tenant,
registerTenant(mt_context, mtservice, clusteradmin_jwt, new_tenant, [ [method:'Host', content:'localhost'], [ method:'Host', content:'alerthub_cc.casp.semweb.co' ] ]);

// just a proxy for create database/user/grant. This step doesn't do anything other than provision a database
println("Provision a new db");
provisionPostgresDatabase(mt_context, mtservice, clusteradmin_jwt, 'alerthub_cc','alhun','alpw');

// Add details of the new datasource to the registry- create a datasource called alerthub_cc pointing at the new db
// This registers the details of the db created in the step above and associates a config name alerthub_cc with that
println("Registering datasource");
registerDataSource(mt_context, mtservice, clusteradmin_jwt, 'alerthub_cc', 'org.postgresql.Driver', 'alhun','alpw','none',
                   'alerthub_cc','localhost','org.hibernate.dialect.PostgreSQL94Dialect');

println("Register tenant alerthub will use datasource alerthub_cc for all versions of capcreator service");
// Create a TenantServiceVersion/DataSource entry for the alerthub tenant, capcreator service, all versions use the alerthub_cc datasource
// This call says that all versions of capcreator for tenant alerthub will use this datastore

// It's important that this string matches the service name (Initially grails app name)
String service_identifier = 'casp_core'
registerTSVDS(mt_context, mtservice, clusteradmin_jwt, new_tenant, service_identifier, '*', 'alerthub_cc')

println("Installing app against datasource");
installOrUpgrade(mt_context,
                 mtservice,
                 clusteradmin_jwt, 
                 new_tenant, 
                 [ admuser:[ username:'admin' ], rootOU:[ code:'TestOrg', name:'My test Org' ] ] );

println("get token for admin account in new tenant ${new_tenant}");
Map cccontext = [
  auth : [
    access_token : login(keycloak, 'holo', new_tenant, 'admin', 'password')
  ]
]
println("cccontext: ${cccontext}");

if (  cccontext.auth.access_token != null ) {
  println("Valid call to /test - should work");
  callTest(mt_context, mtservice, cccontext.auth.access_token, new_tenant);
}

// println("call to /test token valid token for different tenant - should fail");
// callTest(http, cccontext.auth.access_token, 'noten');

// println("call to /test token invalid for valid tenant - should fail");
// callTest(http, '11234', 'alerthub');

// Lets create some organisational units - our OUs are  "TestOrg" which was created by the install endpoint

println("Setting up party entries");
// Set up some organisational units with a hierarchical structure
def pty_org1 = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Org1', 'ORG1',null);
println("Result: ${pty_org1}");
def pty_org2 = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Org2', 'ORG2',null);
def pty_area_lg1 = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Sheffield City Council', 'UKSCC',null);
def pty_scc_libraries = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Sheffield City Council Libraries', 'SCCLib',pty_area_lg1.id);
def pty_ecc_lib = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Ecclesall Library', 'EccLib',pty_scc_libraries.id);
def pty_sta_lib = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Stannington Library', 'StaLib',pty_scc_libraries.id);
def pty_sth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Sheffield Teaching Hospitals', 'UKSTH',null);
def pty_hallamshire = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Sheffield Hallamshire Hospital', 'HHOSP',pty_sth.id);

def pty_usa_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'USA Alerting Authority', 'USA_AA',null);
def pty_usa_ms_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Massachusetts Alerting Authority', 'USA_MS',pty_usa_auth.id);
def pty_usa_ny_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'New York Alerting Authority', 'USA_NY',pty_usa_auth.id);

def pty_europe_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'European Alerting Authority', 'EU_AA',null);
def pty_uk_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'UK Alerting Authority', 'UK_AA',pty_europe_auth.id);
def pty_eng_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'England Alerting Authority', 'UK_EN_AA',pty_uk_auth.id);
def pty_scot_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Scotland Alerting Authority', 'UK_SC_AA',pty_uk_auth.id);
def pty_ire_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Ireland Alerting Authority', 'UK_IE_AA',pty_uk_auth.id);
def pty_wales_auth = createPartyGraphQL(mtservice, cccontext.auth.access_token, 'alerthub', 'Wales Alerting Authority', 'UK_WA_AA',pty_uk_auth.id);

// Create some usergroups
// SCC_ADMIN
// ORG2_ADMIN
// STH_ADMIN
// scc_staff

// Add some users to those usergroups


File f = new File('capcol_graphql_schema.json');
f << getSchema(mtservice, cccontext.auth.access_token, 'alerthub');

println("Create Grants");

//Create a hologroup called scc_staff
println("Attempt to create a group called scc_staff");
createGroupGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'scc_staff');

// By default, the admin user gets access to everything.
// Users in group scc_staff get access to all SCC resources
createGrantGraphql(mtservice, cccontext.auth.access_token, 'alerthub',
                           'SCCLib/%',  // The context being granted - all resources under SCCLib
                           '%',         // What kind of resource (all)
                           null,        // What specific ID (none-only maych owner SCCLib/%)
                           '%',         // What permission (all)
                           'GROUP',     // Granted to what kind of party - USER or GROUP
                           'scc_staff'); // ID of that party

println("Create Alerts");
// println("Create a alertbase via graphql");
createAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'Some other alert', pty_org2.id);
createAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'test one', pty_ecc_lib.id);
createAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'test', pty_scc_libraries.id);
createAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'one test', pty_sta_lib.id);
createAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'emtestbedd', pty_org2.id);

// println("query alertbase Via Graphql");
queryAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', 'test');


// Crunch time
// Log in as user fred who only has 
println("Done");




// This section creates a load of random alerts - and will usually be commented out
/* */
def parties = [ pty_org2.id, pty_area_lg1.id, pty_scc_libraries.id, pty_ecc_lib.id, pty_sta_lib.id, pty_sth.id, pty_hallamshire.id, pty_usa_ms_auth.id, pty_usa_ny_auth.id,
                pty_eng_auth.id, pty_scot_auth.id, pty_ire_auth.id, pty_wales_auth.id ]
def alert_words = [ 'dolphin', 'keyboard', 'cherry', 'phone', 'pen', 'book', 'alert', 'urgent', 'trivial', 'sink', 'flood', 'road', 'weather', 'sequence', 'a', 'fidget', 'house', 'grid', 'broadcast', 'local', 'national', 'regional', 'global', 'hyperlocal', 'air quality', 'NO2', 'PM2.5', 'PM10', 'Fire', 'Authorized', 'Responder' ]

def makeRandomAlert(int n, List words) {
  String result = new Random().with {
    (1..n).collect { words[ nextInt( words.size() ) ] }.join(' ')
  }
  return result;
}

long perf_start = System.currentTimeMillis();

int target = 50;
int ctr = 0;
for ( int iteration=0; iteration<target; iteration++ ) {
  String owning_party = parties.get(new Random().nextInt(parties.size()))
  String alert_title = makeRandomAlert(5,alert_words) + " [${ctr++}]"
  println("Create alert [${ctr}] ${alert_title} for ${owning_party}");
  createAlertViaGraphql(mtservice, cccontext.auth.access_token, 'alerthub', alert_title, owning_party);
}

long elapsed =  System.currentTimeMillis() - perf_start
println("Done elapsed = ${elapsed} / ${elapsed/target}ms per alert");
/* */
