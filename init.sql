select 1;

CREATE USER mtcp WITH PASSWORD 'mtcp';
DROP DATABASE if exists mtcp_registry;
CREATE DATABASE mtcp_registry;
GRANT ALL PRIVILEGES ON DATABASE mtcp_registry to mtcp;

CREATE USER keycloak WITH PASSWORD 'keycloak';
CREATE DATABASE keycloak;
GRANT ALL PRIVILEGES ON DATABASE keycloak to keycloak;

ALTER USER mtcp CREATEDB;
ALTER USER mtcp CREATEROLE;
ALTER USER mtcp WITH SUPERUSER;
CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE USER testtenantg WITH PASSWORD 'testtenantg';
DROP DATABASE if exists testtenantg;
CREATE DATABASE testtenantg;
GRANT ALL PRIVILEGES ON DATABASE testtenantg to testtenantg;

CREATE USER testtenanth WITH PASSWORD 'testtenanth';
DROP DATABASE if exists testtenanth;
CREATE DATABASE testtenanth;
GRANT ALL PRIVILEGES ON DATABASE testtenanth to testtenanth;

CREATE USER testtenantj WITH PASSWORD 'testtenantj';
DROP DATABASE if exists testtenantj;
CREATE DATABASE testtenantj;
GRANT ALL PRIVILEGES ON DATABASE testtenantj to testtenantj;

CREATE USER testtenantk WITH PASSWORD 'testtenantk';
DROP DATABASE if exists testtenantk;
CREATE DATABASE testtenantk;
GRANT ALL PRIVILEGES ON DATABASE testtenantk to testtenantk;

CREATE USER testun WITH PASSWORD 'testpw';
DROP DATABASE if exists test01;
CREATE DATABASE test01;
GRANT ALL PRIVILEGES ON DATABASE test01 to testun;
